import {createApp} from 'vue'
import {createPinia} from 'pinia'
import UserManagementService from "@/pages/UserProfile/Services/UserManagementService";
import MoviesService from "@/pages/MoviesPage/Services/MoviesService";
import {PROVIDE_UMS_KEY, PROVIDE_MOVIES_KEY} from "@/types/types";
import App from './App.vue'
import router from './router'
import './assets/main.css'

const app = createApp(App)
app.use(createPinia())
app.use(router)
app.provide<typeof UserManagementService>(PROVIDE_UMS_KEY,  UserManagementService);
app.provide<typeof MoviesService>(PROVIDE_MOVIES_KEY,  MoviesService);
app.mount('#app')
