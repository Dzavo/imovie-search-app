// @ts-ignore
import {axios, AxiosResponse} from "axios";
interface Movie {
    id: number;
    title: string;
    overview: string;
    poster_path: string;
    release_date: string;
    vote_average: number;
    vote_count: number;
}

interface Video {
    id: string;
    key: string;
    name: string;
    site: string;
    type: string;
}

class TheMovieDB {
    private readonly apiKey: string;
    private readonly baseUrl: string;

    constructor(apiKey: string) {
        this.apiKey = apiKey;
        this.baseUrl = 'https://api.themoviedb.org/3';
    }

    async searchMovies(query: string): Promise<Movie[]> {
        const response: AxiosResponse<any> = await axios.get(
            `${this.baseUrl}/search/movie`,
            {
                params: {
                    api_key: this.apiKey,
                    query,
                },
            }
        );
        return response.data.results;
    }

    async getMovieDetails(movieId: number): Promise<Movie> {
        const response: AxiosResponse<Movie> = await axios.get(
            `${this.baseUrl}/movie/${movieId}`,
            {
                params: {
                    api_key: this.apiKey,
                },
            }
        );
        return response.data;
    }

    async getMovieVideos(movieId: number): Promise<Video[]> {
        const response: AxiosResponse<any> = await axios.get(
            `${this.baseUrl}/movie/${movieId}/videos`,
            {
                params: {
                    api_key: this.apiKey,
                },
            }
        );
        return response.data.results;
    }
}

const tmdb = new TheMovieDB('YOUR_API_KEY');

tmdb.searchMovies('The Dark Knight').then((movies) => {
    console.log(movies);
});

tmdb.getMovieDetails(155).then((movie) => {
    console.log(movie);
});

tmdb.getMovieVideos(155).then((videos) => {
    console.log(videos);
});
