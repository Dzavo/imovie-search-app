export interface Movie {
  imdbID: string
  Title: string
  Type: string
  Poster: string
  Year: number
  isFavorite: boolean
}

export interface MovieDetail {
  imdbID: string
  Title: string
  Year: string
  Rated: string
  Released: string
  Runtime: string
  Genre: string
  Director: string
  Writer: string
  Actors: string
  Plot: string
  Language: string
  Country: string
  Awards: string
  Poster: string
  Ratings: { Source: string; Value: string }[]
  Metascore: string
  imdbRating: string
  imdbVotes: string
  Type: string
  DVD: string
  BoxOffice: string
  Production: string
  Website: string
  Response: string
  isFavorite: boolean
}

export const PROVIDE_UMS_KEY = 'userManagementServiceInjectionKey';
export const PROVIDE_MOVIES_KEY = 'moviesServiceInjectionKey';
