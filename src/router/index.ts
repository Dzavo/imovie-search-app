import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../pages/HomePage/HomePage.vue'

export enum RoutesEnum {
  INTRODUCTION_PAGE = 'INTRODUCTION_PAGE',
  HOME_PAGE = 'HOME_PAGE',
  LOGIN_PAGE = 'LOGIN_PAGE',
  REGISTRATION_PAGE = 'REGISTRATION_PAGE',
  USER_PROFILE_PAGE = 'USER_PROFILE_PAGE',

  MOVIES_PAGE= 'MOVIES_PAGE',
  PREVIEW_MOVIE = 'PREVIEW_MOVIE',
  WATCH_LIST = 'WATCH_LIST',
  STREAMING_PLATFORM = 'STREAMING_PLATFORM',
  THANK_YOU_PAGE = 'THANK_YOU_PAGE',
}

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: RoutesEnum.HOME_PAGE,
      component: HomeView
    },
    {
      path: '/login',
      name: RoutesEnum.LOGIN_PAGE,
      component: () => import('../pages/UserRegistration/UserLoginPage.vue')
    },
    {
      path: '/movies',
      name: RoutesEnum.MOVIES_PAGE,
      component: () => import('../pages/MoviesPage/MoviesPage.vue')
    },
    {
      path: '/preview',
      name: RoutesEnum.PREVIEW_MOVIE,
      component: () => import('../pages/PreviewMoviePage/PreviewMoviePage.vue')
    },
    {
      path: '/watch-list',
      name: RoutesEnum.WATCH_LIST,
      component: () => import('../pages/WatchListPage/WatchListPage.vue')
    },
    {
      path: '/streaming-platform',
      name: RoutesEnum.STREAMING_PLATFORM,
      component: () => import('../pages/StreamingPlatforms/StreamingPlatformsPage.vue')
    },
    {
      path: '/user-profile',
      name: RoutesEnum.USER_PROFILE_PAGE,
      component: () => import('../pages/UserProfile/UserProfilePage.vue')
    },
  ]
})

export default router
