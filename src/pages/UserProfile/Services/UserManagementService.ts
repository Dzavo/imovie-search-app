import { reactive } from 'vue';
import {testUsersArray} from "@/data/data";
export interface User {
    email: string;
    password: string;
    firstName?: string;
    lastName?: string;
}
class UserManagementService {
    private user: User | null | undefined;
    private users: User[] = [];
    constructor() {
        const storedUser = localStorage.getItem('user');
        this.users = testUsersArray;

        if (storedUser) {
            this.user = JSON.parse(storedUser);
        }
    }
    register(user: User): void {
        this.users.push(user);
        localStorage.setItem('users', JSON.stringify(this.users));
        this.login(user);
    }
    login(user: User): boolean {
        const loggedUser = this.users.find((userObject) =>
            user.email === userObject.email && user.password === userObject.password);

        if (loggedUser) {
            this.user = loggedUser;
            localStorage.setItem('user', JSON.stringify(this.user));
            return true;
        } else {
            console.log('Invalid email or password');
            return false;
        }
    }

    get userLength():number{
        return this.users.length;
    }

    logout(): void {
        this.user = null;
        localStorage.removeItem('user');
    }
    getCurrentLoggedUser(): User {
        return <User>reactive(this.user!);
    }

    get userIsLogged(): boolean {
        return !!this.user;
    }

    get currentUserFullName(): string {
        return `${this.user?.firstName} ${this.user?.lastName}`;
    }
    addUser(user: User) {
        this.users.push(user);
        localStorage.setItem('users', JSON.stringify(this.users));
    }
    removeUser(username: string) {
        this.users = this.users.filter(user => user.email !== username);
        localStorage.setItem('users', JSON.stringify(this.users));
    }
}

export default new UserManagementService();
