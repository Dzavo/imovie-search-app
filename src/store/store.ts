import {defineStore} from 'pinia';
import type {Movie} from "@/types/types";
import type {User} from "@/pages/UserProfile/Services/UserManagementService";
import userManagementService from "@/pages/UserProfile/Services/UserManagementService";

interface State {
  serverVersion: string;
  selectedMovie: Movie | null,
  favoritesMovie: Movie | null,
  currentUser: User | undefined,
}
export const useStore = defineStore('store', {
  state: (): State => {
    return {
      serverVersion: '0.2.3.2',
      selectedMovie: null,
      favoritesMovie: null,
      currentUser: userManagementService.getCurrentLoggedUser(),
    };
  },
  getters: {
    getServerVersion: (state) => state.serverVersion,
    getMovie:(state): Movie | null => state.selectedMovie,
    getCurrentUser(state): User | undefined {
      return state.currentUser;
    },
  },
  actions: {
    selectMovie(movie: Movie){
      this.$state.selectedMovie = movie;
      console.log(this.$state);
    },
  },
});
