import type {Movie, MovieDetail} from '@/types/types'
import {reactive} from "vue";
import {isArray} from "lodash-es";

export class MovieService {
  private readonly _searchedMovies: string [];
  private readonly apiKey: string
  private readonly apiUrl: string = import.meta.env.VITE_MOVIES_API_URL
  private favoriteMoviesKey:string = 'favoriteMovies';
  private storageFavoritesMovies : Movie [];
  public movies : Movie[];


  constructor(apiKey: string) {
    this.apiKey = apiKey;
    // if(!this.apiKey){
    //   this.movies = testOfflineMoviesArray;
    // }
    this.storageFavoritesMovies = this.getFavoritesMovies()
    this.movies = reactive([]);
    this._searchedMovies = reactive(JSON.parse(localStorage.getItem('searchedMovies')  || '[]'));
  }

  async getPopularMovies(keyword: string): Promise<Movie[]> {
    const response = await fetch(`${this.apiUrl}/?apikey=${this.apiKey}&s=${keyword}&page=1`)
    const data = await response.json()
    if (data.Response === 'True') {
      const results = data['Search'] as Movie[]
      const moviesArray =  results.map((movie: Movie) => ({
        imdbID: movie.imdbID,
        Title: movie.Title,
        Type: movie.Type,
        Year: movie.Year,
        Poster: movie.Poster,
        isFavorite: this.favoritesMovies?.some((f) => f?.imdbID === movie.imdbID) || false
      }))
      this.movies = moviesArray;
      return moviesArray;
    } else {
      return [];
    }
  }

  get searchedMovies(): string[] {
    return this._searchedMovies;
  }
  setSearchedMovie(searchedFor: string):void {
    const movieName = searchedFor.trim().toLowerCase();
    if(!isArray(this._searchedMovies)) return;
    if (!this._searchedMovies.includes(movieName)) {
      this._searchedMovies.push(movieName);
      localStorage.setItem('searchedMovies', JSON.stringify(this.searchedMovies));
    }
  }

  get favoritesMoviesCount():number{
    return this.favoritesMovies.length;
  }

  get favoritesMovies(){
    return this.storageFavoritesMovies;
  }
  async getMovieDetails(movie: Movie): Promise<MovieDetail> {
    const result = await fetch(`${this.apiUrl}/?i=${movie.imdbID}&apikey=${this.apiKey}`)
    return await result.json()
  }

  getFavoritesMovies(): Movie[] {
    const favorites = localStorage.getItem(this.favoriteMoviesKey);

    if (favorites) {
      return JSON.parse(favorites);
    } else {
      return [];
    }
  }

  addMovieToFavouritesById(id:string){
    const favorites = this.favoritesMovies;
    const movieIndex = this.movies.findIndex((m: Movie) => m.imdbID === id);
    if (movieIndex !== -1) {
      favorites.push(this.movies[movieIndex]);
      localStorage.setItem(this.favoriteMoviesKey, JSON.stringify(favorites));
      this.storageFavoritesMovies = favorites;
    }
    this.movies[movieIndex].isFavorite = true;
  }

  addMovieToFavourites(movie: Movie): void {
    const favorites = this.getFavoritesMovies();
    const favoritesIndex = favorites.findIndex((m: Movie) => m.imdbID === movie.imdbID)
    if (favoritesIndex === -1) {
      favorites.push(movie);
      localStorage.setItem(this.favoriteMoviesKey, JSON.stringify(favorites));
      this.storageFavoritesMovies = favorites;
    }
  }

  removeMovieFromFavorites(movie: Movie): void {
    const favorites = this.getFavoritesMovies();
    const favoritesIndex = favorites.findIndex((m: Movie) => m.imdbID === movie.imdbID);

    if (favoritesIndex !== -1) {
      favorites.splice(favoritesIndex, 1);
      localStorage.setItem(this.favoriteMoviesKey, JSON.stringify(favorites));
      this.storageFavoritesMovies = favorites;
      this.movies[favoritesIndex].isFavorite = false;
    }
  }

}
const apiKey = import.meta.env.VITE_API_KEY
export default new MovieService(apiKey);
