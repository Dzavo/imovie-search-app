import type {User} from "@/pages/UserProfile/Services/UserManagementService";

export const streamingPlatforms =  [
    {
        name: 'Netflix',
        logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Netflix_2015_logo.svg/2560px-Netflix_2015_logo.svg.png',
        subscription: 'Paid',
        exclusiveContent: true,
        freeTrial: true
    },
    {
        name: 'HBO Max',
        logo: 'https://cdn.alza.cz/Foto/ImgGalery/Image/hbo-max-logo.jpg',
        subscription: 'Paid',
        exclusiveContent: true,
        freeTrial: false
    },
    {
        name: 'Disney+',
        logo: 'https://logo-download.com/wp-content/data/images/png/Disney+-logo.png',
        subscription: 'Paid',
        exclusiveContent: true,
        freeTrial: false
    },
    {
        name: 'Hulu',
        logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Hulu_Logo.svg/1200px-Hulu_Logo.svg.png',
        subscription: 'Paid',
        exclusiveContent: false,
        freeTrial: true
    },
    {
        name: 'Amazon Prime Video',
        logo: 'https://m.media-amazon.com/images/G/01/primevideo/seo/primevideo-seo-logo.png',
        subscription: 'Paid',
        exclusiveContent: true,
        freeTrial: true
    }
]

export const testUser = {
    name: 'John Doe',
    email: 'john.doe@example.com',
    location: 'New York, USA',
    moviesWatched: 150,
    topGenre: 'Action',
    favoriteMovie: 'The Dark Knight',
    profilePhoto: 'https://via.placeholder.com/150',
    timeSpentWatching: 2550, // minutes
    favoriteMovies: [
        { title: 'The Shawshank Redemption', rating: 9.3 },
        { title: 'The Godfather', rating: 9.2 },
        { title: 'The Godfather: Part II', rating: 9.0 },
        { title: 'The Dark Knight', rating: 9.0 },
        { title: '12 Angry Men', rating: 8.9 }
    ],
    recentActivity: [
        {
            id: 1,
            date: '2022-03-10',
            title: 'Watched The Matrix',
            duration: 120,
            description: 'I watched The Matrix for the first time and loved it!'
        },
        {
            id: 2,
            date: '2022-03-08',
            title: 'Commented on a post',
            duration: 5,
            description: 'Left a comment on a post about the latest movie releases.'
        },
        {
            id: 3,
            date: '2022-03-05',
            title: 'Liked a video',
            duration: 1,
            description: 'Liked a video about the making of a new movie.'
        }
    ],
    friends: [
        {
            id:1,
            name: 'Jane Smith',
            location: 'Los Angeles, USA',
            profilePhoto: 'https://via.placeholder.com/150',
        },
        {
            id:2,
            name: 'Bob Johnson',
            location: 'Chicago, USA',
            profilePhoto: 'https://via.placeholder.com/150',
        },
        // Add more friends here
    ],
    followers: [
        {
            id:1,
            name: 'Emily Davis',
            location: 'San Francisco, USA',
            profilePhoto: 'https://via.placeholder.com/150',
        },
        {
            id:2,
            name: 'Tom Wilson',
            location: 'Seattle, USA',
            profilePhoto: 'https://via.placeholder.com/150',
        },
        // Add more followers here
    ],
}

export const testUsersArray : User []= [
    {
        "email": "test@test.com",
        "password": "test",
        "firstName": "Test",
        "lastName": "Admin"
    },
    {
        "email": "martin.dzavik@test.com",
        "password": "123",
        "firstName": "Main",
        "lastName": "Admin"
    },
    {
        "email": "john.doe@example.com",
        "password": "password123",
        "firstName": "Alice",
        "lastName": "Johnson"
    },
    {
        "email": "jane.doe@example.com",
        "password": "qwerty123",
        "firstName": "Bob",
        "lastName": "Williams"
    },
    {
        "email": "bob.smith@example.com",
        "password": "ilovecoding",
        "firstName": "Mary",
        "lastName": "Davis"
    },
    {
        "email": "sara.johnson@example.com",
        "password": "letmein",
        "firstName": "David",
        "lastName": "Miller"
    },
    {
        "email": "mike.brown@example.com",
        "password": "mypass",
        "firstName": "Emily",
        "lastName": "Jones"
    },
    {
        "email": "jenny.nguyen@example.com",
        "password": "mypassword",
        "firstName": "Brian",
        "lastName": "Taylor"
    },
    {
        "email": "david.jackson@example.com",
        "password": "password123",
        "firstName": "Julia",
        "lastName": "Anderson"
    },
    {
        "email": "olivia.white@example.com",
        "password": "mypassword123",
        "firstName": "Michael",
        "lastName": "Wilson"
    },
    {
        "email": "megan.carter@example.com",
        "password": "ilovecoding",
        "firstName": "Sophie",
        "lastName": "Robinson"
    },
    {
        "email": "chris.evans@example.com",
        "password": "avengersassemble",
        "firstName": "Tom",
        "lastName": "Thompson"
    }
]
// export const testOfflineMoviesArray = [{"imdbID":"tt0372784","Title":"Batman Begins","Type":"movie","Year":"2005","Poster":"https://m.media-amazon.com/images/M/MV5BOTY4YjI2N2MtYmFlMC00ZjcyLTg3YjEtMDQyM2ZjYzQ5YWFkXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg","isFavorite":false},{"imdbID":"tt2975590","Title":"Batman v Superman: Dawn of Justice","Type":"movie","Year":"2016","Poster":"https://m.media-amazon.com/images/M/MV5BYThjYzcyYzItNTVjNy00NDk0LTgwMWQtYjMwNmNlNWJhMzMyXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg","isFavorite":true},{"imdbID":"tt1877830","Title":"The Batman","Type":"movie","Year":"2022","Poster":"https://m.media-amazon.com/images/M/MV5BMDdmMTBiNTYtMDIzNi00NGVlLWIzMDYtZTk3MTQ3NGQxZGEwXkEyXkFqcGdeQXVyMzMwOTU5MDk@._V1_SX300.jpg","isFavorite":false},{"imdbID":"tt0096895","Title":"Batman","Type":"movie","Year":"1989","Poster":"https://m.media-amazon.com/images/M/MV5BZDNjOGNhN2UtNmNhMC00YjU4LWEzMmUtNzRkM2RjN2RiMjc5XkEyXkFqcGdeQXVyMTU0OTM5ODc1._V1_SX300.jpg","isFavorite":false},{"imdbID":"tt0103776","Title":"Batman Returns","Type":"movie","Year":"1992","Poster":"https://m.media-amazon.com/images/M/MV5BOGZmYzVkMmItM2NiOS00MDI3LWI4ZWQtMTg0YWZkODRkMmViXkEyXkFqcGdeQXVyODY0NzcxNw@@._V1_SX300.jpg","isFavorite":false},{"imdbID":"tt0118688","Title":"Batman & Robin","Type":"movie","Year":"1997","Poster":"https://m.media-amazon.com/images/M/MV5BMGQ5YTM1NmMtYmIxYy00N2VmLWJhZTYtN2EwYTY3MWFhOTczXkEyXkFqcGdeQXVyNTA2NTI0MTY@._V1_SX300.jpg","isFavorite":false},{"imdbID":"tt0112462","Title":"Batman Forever","Type":"movie","Year":"1995","Poster":"https://m.media-amazon.com/images/M/MV5BNDdjYmFiYWEtYzBhZS00YTZkLWFlODgtY2I5MDE0NzZmMDljXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg","isFavorite":false},{"imdbID":"tt4116284","Title":"The Lego Batman Movie","Type":"movie","Year":"2017","Poster":"https://m.media-amazon.com/images/M/MV5BMTcyNTEyOTY0M15BMl5BanBnXkFtZTgwOTAyNzU3MDI@._V1_SX300.jpg","isFavorite":false},{"imdbID":"tt0103359","Title":"Batman: The Animated Series","Type":"series","Year":"1992–1995","Poster":"https://m.media-amazon.com/images/M/MV5BOTM3MTRkZjQtYjBkMy00YWE1LTkxOTQtNDQyNGY0YjYzNzAzXkEyXkFqcGdeQXVyOTgwMzk1MTA@._V1_SX300.jpg","isFavorite":false},{"imdbID":"tt18689424","Title":"Batman v Superman: Dawn of Justice (Ultimate Edition)","Type":"movie","Year":"2016","Poster":"https://m.media-amazon.com/images/M/MV5BN2I4OTllM2MtMWVhNC00MjkzLWJlMDUtN2FhMGQ2ZGVjMjllXkEyXkFqcGdeQXVyMTEyNzgwMDUw._V1_SX300.jpg","isFavorite":false}]
