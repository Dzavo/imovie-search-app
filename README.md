# iMovie Search App
Simple movie application for login, search, adding and removing movies from favorites.
In this current state it for example purposes and look only.

Some pages from project are not completed yet and the project is still under development.
(UserProfile, registration, etc..)
Next possible features will be added: i18n translations,videos, trailers, classic/dark theme, web socket messaging
between users, etc...
API will be work, when deploy completed - but CI/CD not completed yet (it will be added later).

Logins for test purpose:
```sh
username:test@test.com
password: test123
```
## Project Setup

```sh
npm install
```

### Run the project in DEV mode

```sh
npm run dev
```
# Some preview images of the project are below:

<img src="src/assets/app_screens/01.png" alt="INTRO" width="250" height="200">
<img src="src/assets/app_screens/02_movies.png" alt="Search movie" width="250" height="200">
<img src="src/assets/app_screens/03_detail.png" alt="Movie detail" width="250" height="200">
<img src="src/assets/app_screens/04_favorites.png" alt="List of favorites" width="250" height="200">
